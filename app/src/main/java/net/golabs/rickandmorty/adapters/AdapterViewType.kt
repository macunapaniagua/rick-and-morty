package net.golabs.rickandmorty.adapters

enum class AdapterViewType {
    GRID,
    LIST
}