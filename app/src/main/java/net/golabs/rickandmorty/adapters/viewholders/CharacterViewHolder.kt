package net.golabs.rickandmorty.adapters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import net.golabs.rickandmorty.models.ui.CharacterUiModel

abstract class CharacterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    abstract fun bindViews(item: CharacterUiModel, clickListener: (Int) -> Unit)
}