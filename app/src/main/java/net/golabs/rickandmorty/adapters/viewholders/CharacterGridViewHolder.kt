package net.golabs.rickandmorty.adapters.viewholders

import net.golabs.rickandmorty.databinding.GalleryGridItemBinding
import net.golabs.rickandmorty.models.ui.CharacterUiModel
import net.golabs.rickandmorty.utils.extensions.loadImage

class CharacterGridViewHolder(
    private val binding: GalleryGridItemBinding
) : CharacterViewHolder(binding.root) {

    override fun bindViews(item: CharacterUiModel, clickListener: (Int) -> Unit) = with(binding) {
        iconImage.loadImage(item.imageUrl)
        primaryDescriptionText.text = item.primaryDescription
        root.setOnClickListener { clickListener.invoke(item.uid) }
    }

}