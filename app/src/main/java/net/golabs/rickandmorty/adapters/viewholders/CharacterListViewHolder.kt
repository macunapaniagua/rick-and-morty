package net.golabs.rickandmorty.adapters.viewholders

import net.golabs.rickandmorty.databinding.GalleryListItemBinding
import net.golabs.rickandmorty.models.ui.CharacterUiModel
import net.golabs.rickandmorty.utils.extensions.loadCircleImage

class CharacterListViewHolder(
    private val binding: GalleryListItemBinding
) : CharacterViewHolder(binding.root) {

    override fun bindViews(item: CharacterUiModel, clickListener: (Int) -> Unit) = with(binding) {
        iconImage.loadCircleImage(item.imageUrl)
        primaryDescriptionText.text = item.primaryDescription
        secondaryDescriptionText.text = item.secondaryDescription
        root.setOnClickListener { clickListener.invoke(item.uid) }
    }

}