package net.golabs.rickandmorty.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import net.golabs.rickandmorty.adapters.viewholders.CharacterGridViewHolder
import net.golabs.rickandmorty.adapters.viewholders.CharacterListViewHolder
import net.golabs.rickandmorty.adapters.viewholders.CharacterViewHolder
import net.golabs.rickandmorty.databinding.GalleryGridItemBinding
import net.golabs.rickandmorty.databinding.GalleryListItemBinding
import net.golabs.rickandmorty.models.ui.CharacterUiModel
import net.golabs.rickandmorty.utils.CharactersDiffUtil


class CharactersAdapter(
    private var items: MutableList<CharacterUiModel>,
    var adapterViewType: AdapterViewType,
    private val clickListener: (Int) -> Unit
) : RecyclerView.Adapter<CharacterViewHolder>() {

    constructor(clickListener: (Int) -> Unit) : this(mutableListOf(), AdapterViewType.GRID, clickListener)

    override fun getItemCount(): Int = items.size

    override fun getItemViewType(position: Int): Int {
        return adapterViewType.ordinal
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == AdapterViewType.LIST.ordinal) {
            val binding = GalleryListItemBinding.inflate(inflater, parent, false)
            CharacterListViewHolder(binding)
        } else {
            val binding = GalleryGridItemBinding.inflate(inflater, parent, false)
            CharacterGridViewHolder(binding)
        }
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        holder.bindViews(items[position], clickListener)
    }

    fun setItems(newItems: List<CharacterUiModel>) {
        val diffResult = DiffUtil.calculateDiff(CharactersDiffUtil(items, newItems))
        diffResult.dispatchUpdatesTo(this)
        items = newItems.toMutableList()
    }

    fun addItems(newItems: List<CharacterUiModel>) {
        val oldListSize = itemCount
        items.addAll(newItems)
        notifyItemRangeChanged(oldListSize, itemCount)
    }

}