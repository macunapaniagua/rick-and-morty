package net.golabs.rickandmorty.viewmodels

import androidx.lifecycle.*
import net.golabs.rickandmorty.database.entities.Character
import net.golabs.rickandmorty.repositories.CharactersRepository
import net.golabs.rickandmorty.models.ui.CharacterDetailsUiModel
import net.golabs.rickandmorty.utils.mappers.toCharacterDetailsUiModel

class CharacterDetailsViewModel(
    charactersRepository: CharactersRepository,
    savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val characterDetails: LiveData<Character>

    init {
        val uid: Int = savedStateHandle["characterUid"] ?: throw IllegalArgumentException("missing uid")
        characterDetails = charactersRepository.getCharacterByUid(uid)
    }

    val uiModel: LiveData<CharacterDetailsUiModel> = characterDetails.map { characterDetails ->
        characterDetails.toCharacterDetailsUiModel()
    }

}