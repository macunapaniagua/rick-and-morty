package net.golabs.rickandmorty.viewmodels.factories

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import net.golabs.rickandmorty.repositories.CharactersRepository
import net.golabs.rickandmorty.viewmodels.CharacterDetailsViewModel

class CharacterDetailsViewModelFactory(
    private val charactersRepository: CharactersRepository,
    owner: SavedStateRegistryOwner,
    defaultArgs: Bundle? = null
): AbstractSavedStateViewModelFactory(owner, defaultArgs) {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T {
        return CharacterDetailsViewModel(charactersRepository, handle) as T
    }

}