package net.golabs.rickandmorty.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.switchMap
import net.golabs.rickandmorty.models.ui.CharacterUiModel
import net.golabs.rickandmorty.repositories.CharactersRepository
import net.golabs.rickandmorty.utils.Result
import net.golabs.rickandmorty.utils.mappers.toCharacterUiModels

class CharactersViewModel(private val charactersRepository: CharactersRepository) : ViewModel() {

    private var nextPage: Int? = null
    private val page = MutableLiveData(FIRST_PAGE)
    private val charactersCollection: MutableList<CharacterUiModel> = mutableListOf()

    val uiModels: LiveData<Result<List<CharacterUiModel>>> = page.switchMap { page ->
        charactersRepository.getAllCharacters(page).map { result ->
            val uiResult: Result<List<CharacterUiModel>> = when (result) {
                is Result.Loading -> Result.Loading()
                is Result.Success -> {
                    result.data!!.run {
                        this@CharactersViewModel.nextPage = this.nextPage
                        charactersCollection.addAll(items.toCharacterUiModels())
                        Result.Success(charactersCollection.toList())
                    }
                }
                is Result.Error -> Result.Error(result.throwable!!)
            }
            uiResult
        }
    }

    fun tryAgain() {
        page.value = page.value
    }

    fun loadMoreCharacters() {
        nextPage?.let {
            page.value = it
        }
    }

    companion object {
        private const val FIRST_PAGE = 1
    }

}