package net.golabs.rickandmorty.viewmodels.factories

import android.content.Context
import androidx.fragment.app.Fragment
import net.golabs.rickandmorty.database.RickAndMortyDatabase
import net.golabs.rickandmorty.network.WebService
import net.golabs.rickandmorty.repositories.CharactersRepository

object ViewModelFactoryProvider {

    private fun getCharactersRepository(context: Context): CharactersRepository {
        return CharactersRepository(
            webService = WebService.create(),
            characterDao = RickAndMortyDatabase.getDataBase(context.applicationContext).characterDao()
        )
    }

    fun provideCharacterDetailsViewModelFactory(fragment: Fragment): CharacterDetailsViewModelFactory {
        return CharacterDetailsViewModelFactory(
            charactersRepository = getCharactersRepository(fragment.requireContext()),
            owner = fragment,
            defaultArgs = fragment.arguments
        )
    }

    fun provideCharacterViewModelFactory(context: Context): CharactersViewModelFactory {
        return CharactersViewModelFactory(
            charactersRepository = getCharactersRepository(context)
        )
    }

}