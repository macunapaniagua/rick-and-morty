package net.golabs.rickandmorty.viewmodels.factories

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.golabs.rickandmorty.repositories.CharactersRepository
import net.golabs.rickandmorty.viewmodels.CharactersViewModel

class CharactersViewModelFactory(
    private val charactersRepository: CharactersRepository
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CharactersViewModel(charactersRepository) as T
    }
}