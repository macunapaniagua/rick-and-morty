package net.golabs.rickandmorty.models.domain

data class PagedList<T>(
    val currentPage: Int,
    val nextPage: Int?,
    val items: List<T>
)