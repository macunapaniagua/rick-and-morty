package net.golabs.rickandmorty.models.ui

class CharacterUiModel(
    val uid: Int,
    val imageUrl: String,
    val primaryDescription: String,
    val secondaryDescription: String
)