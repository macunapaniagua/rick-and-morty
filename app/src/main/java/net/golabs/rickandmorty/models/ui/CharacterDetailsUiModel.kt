package net.golabs.rickandmorty.models.ui

import net.golabs.rickandmorty.database.entities.Place

data class CharacterDetailsUiModel(
    val uid: Int,
    val name: String,
    val imageUrl: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val episodes: List<String>,
    val origin: Place,
    val location: Place
)