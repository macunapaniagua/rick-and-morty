package net.golabs.rickandmorty.screens.characters

import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.golabs.rickandmorty.R
import net.golabs.rickandmorty.adapters.AdapterViewType
import net.golabs.rickandmorty.adapters.CharactersAdapter
import net.golabs.rickandmorty.databinding.CharactersFragmentBinding
import net.golabs.rickandmorty.utils.ItemOffsetDecoration
import net.golabs.rickandmorty.utils.Result
import net.golabs.rickandmorty.viewmodels.CharactersViewModel
import net.golabs.rickandmorty.viewmodels.factories.ViewModelFactoryProvider

class CharactersFragment : Fragment(), MenuProvider {

    private lateinit var menu: Menu
    private lateinit var adapter: CharactersAdapter
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var binding: CharactersFragmentBinding

    private val viewModel: CharactersViewModel by viewModels {
        ViewModelFactoryProvider.provideCharacterViewModelFactory(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = CharactersFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menuInflater.inflate(R.menu.gallery_menu, menu)
        this.menu = menu
        refreshMenuOptions()
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            R.id.gridMenuButton -> {
                changeListToGridView()
                true
            }
            R.id.listMenuButton -> {
                changeListToLinearView()
                true
            }
            else -> false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (requireActivity() as MenuHost).addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        initViews()
        initViewModelObservers()
        setListeners()
    }

    private fun initViews() {
        adapter = CharactersAdapter(::navigateToDetails)
        layoutManager = GridLayoutManager(context, getGridLayoutSpanCount())
        with(binding.charactersList) {
            adapter = this@CharactersFragment.adapter
            layoutManager = this@CharactersFragment.layoutManager
            addItemDecoration(ItemOffsetDecoration(resources.getDimensionPixelSize(R.dimen.gallery_items_offset)))
        }
    }

    private fun setListeners() {
        binding.charactersTryAgain.setOnClickListener { viewModel.tryAgain() }
    }

    private val endlessScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            val itemCount = adapter.itemCount
            val lastItemVisible = layoutManager.findLastVisibleItemPosition()
            if (itemCount == lastItemVisible + 1) {
                viewModel.loadMoreCharacters()
            }
        }
    }

    private fun initViewModelObservers() = with(binding) {
        viewModel.uiModels.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Loading -> {
                    charactersErrorGroup.isVisible = false
                    charactersList.isVisible = adapter.itemCount > 0
                    charactersProgress.isVisible = adapter.itemCount == 0
                    charactersList.removeOnScrollListener(endlessScrollListener)
                }

                is Result.Success -> {
                    charactersProgress.isVisible = false
                    charactersList.isVisible = result.data!!.isNotEmpty()
                    charactersErrorGroup.isVisible = result.data.isEmpty()
                    charactersList.addOnScrollListener(endlessScrollListener)
                    adapter.setItems(result.data)
                }

                is Result.Error -> {
                    charactersList.isVisible = false
                    charactersProgress.isVisible = false
                    charactersErrorGroup.isVisible = true
                    charactersErrorText.text = result.throwable!!.message
                    charactersList.removeOnScrollListener(endlessScrollListener)
                }
            }
        }
    }

    private fun getGridLayoutSpanCount(): Int {
        return if (resources.configuration.orientation == Configuration.ORIENTATION_PORTRAIT) {
            GRID_ITEMS_PER_ROW_PORTRAIT
        } else {
            GRID_ITEMS_PER_ROW_LANDSCAPE
        }
    }

    private fun changeListToLinearView() {
        layoutManager.spanCount = LINEAR_ITEMS_PER_ROW
        (binding.charactersList.adapter as CharactersAdapter).adapterViewType = AdapterViewType.LIST
        refreshMenuOptions()
    }

    private fun changeListToGridView() {
        layoutManager.spanCount = getGridLayoutSpanCount()
        (binding.charactersList.adapter as CharactersAdapter).adapterViewType = AdapterViewType.GRID
        refreshMenuOptions()
    }

    private fun refreshMenuOptions() {
        val showListButton = layoutManager.spanCount != LINEAR_ITEMS_PER_ROW
        menu.findItem(R.id.listMenuButton).isVisible = showListButton
        menu.findItem(R.id.gridMenuButton).isVisible = !showListButton
    }

    private fun navigateToDetails(uid: Int) {
        val directions = CharactersFragmentDirections.actionCharactersDestToCharacterDetailsDest(characterUid = uid)
        findNavController().navigate(directions)
    }

    companion object {
        private const val LINEAR_ITEMS_PER_ROW = 1
        private const val GRID_ITEMS_PER_ROW_PORTRAIT = 3
        private const val GRID_ITEMS_PER_ROW_LANDSCAPE = 5
    }
}