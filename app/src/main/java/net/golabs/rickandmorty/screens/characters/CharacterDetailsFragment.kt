package net.golabs.rickandmorty.screens.characters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import net.golabs.rickandmorty.databinding.CharacterDetailsFragmentBinding
import net.golabs.rickandmorty.models.ui.CharacterDetailsUiModel
import net.golabs.rickandmorty.utils.extensions.loadCircleImage
import net.golabs.rickandmorty.viewmodels.CharacterDetailsViewModel
import net.golabs.rickandmorty.viewmodels.factories.ViewModelFactoryProvider

class CharacterDetailsFragment : DialogFragment() {

    private lateinit var binding: CharacterDetailsFragmentBinding

    private val viewModel: CharacterDetailsViewModel by viewModels {
        ViewModelFactoryProvider.provideCharacterDetailsViewModelFactory(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)
        viewModel.uiModel.observe(viewLifecycleOwner) {
            loadCharacterDetails(it)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = CharacterDetailsFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun loadCharacterDetails(details: CharacterDetailsUiModel) = with(binding) {
        characterDetailsImage.loadCircleImage(details.imageUrl)
        characterDetailsNameText.text = details.name
        characterDetailsStatusText.text = details.status
        with(details.species) {
            characterDetailsSpeciesText.text = this
            characterDetailsSpeciesGroup.isVisible = this.isNotEmpty()
        }
        with(details.type) {
            characterDetailsTypeText.text = this
            characterDetailsTypeGroup.isVisible = this.isNotEmpty()
        }
        with(details.gender) {
            characterDetailsGenderText.text = this
            characterDetailsGenderGroup.isVisible = this.isNotEmpty()
        }
        with(details.origin.name) {
            characterDetailsOriginText.text = this
            characterDetailsOriginGroup.isVisible = this.isNotEmpty()
        }
        with(details.location.name) {
            characterDetailsLastKnownLocationText.text = this
            characterDetailsLastKnownLocationGroup.isVisible = this.isNotEmpty()
        }
    }

}