package net.golabs.rickandmorty.screens

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import net.golabs.rickandmorty.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }
}