package net.golabs.rickandmorty.network

import net.golabs.rickandmorty.network.dtos.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WebService {

    @GET("character")
    fun getAllCharacters(@Query("page") page: Int): Call<AllCharactersDto>

    @GET("character/{id}")
    fun getCharacter(@Path("id") id: Int): Call<CharacterDto>

    companion object {

        private const val BASE_URL = "https://rickandmortyapi.com/api/"

        fun create(): WebService {
            val logger = HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BASIC
            }

            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()

            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(WebService::class.java)
        }

    }
}