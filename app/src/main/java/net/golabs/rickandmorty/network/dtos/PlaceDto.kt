package net.golabs.rickandmorty.network.dtos

data class PlaceDto(
    val name: String,
    val url: String
)