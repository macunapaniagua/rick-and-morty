package net.golabs.rickandmorty.network.dtos

data class InfoDto(
    val count: Int,
    val pages: Int,
    val next: String?,
    val prev: String?
)