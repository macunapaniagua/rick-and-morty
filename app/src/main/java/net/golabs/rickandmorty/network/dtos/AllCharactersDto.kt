package net.golabs.rickandmorty.network.dtos

data class AllCharactersDto(
    val info: InfoDto,
    val results: List<CharacterDto>
)
