package net.golabs.rickandmorty.network.dtos

import com.google.gson.annotations.SerializedName

data class CharacterDto(
    val id: Int,
    val name: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val origin: PlaceDto,
    val location: PlaceDto,
    val image: String,
    @SerializedName("episode")
    val episodes: List<String>,
    val url: String,
    val created: String
)
