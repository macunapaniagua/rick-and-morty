package net.golabs.rickandmorty.utils.mappers

import net.golabs.rickandmorty.database.entities.Place
import net.golabs.rickandmorty.network.dtos.PlaceDto

fun PlaceDto.toPlace(): Place {
    return Place(
        name = name,
        placeUrl = url
    )
}