package net.golabs.rickandmorty.utils.extensions

import android.widget.ImageView
import com.bumptech.glide.Glide
import net.golabs.rickandmorty.R

fun ImageView.loadImage(imageUrl: String) {
    Glide.with(context)
        .load(imageUrl)
        .error(R.drawable.default_image)
        .into(this)
}

fun ImageView.loadCircleImage(imageUrl: String) {
    Glide.with(context)
        .load(imageUrl)
        .error(R.drawable.default_image)
        .circleCrop()
        .into(this)
}