package net.golabs.rickandmorty.utils.mappers

import net.golabs.rickandmorty.database.entities.Character
import net.golabs.rickandmorty.network.dtos.CharacterDto
import net.golabs.rickandmorty.models.ui.CharacterDetailsUiModel
import net.golabs.rickandmorty.models.ui.CharacterUiModel

fun List<CharacterDto>.toCharacters(): List<Character> {
    return this.map { it.toCharacter() }
}

fun CharacterDto.toCharacter(): Character {
    return Character(
        uid = id,
        name = name,
        imageUrl = image,
        status = status,
        species = species,
        type = type,
        gender = gender,
        episodes = episodes,
        origin = origin.toPlace(),
        location = location.toPlace()
    )
}

fun List<Character>.toCharacterUiModels(): List<CharacterUiModel> {
    return this.map { it.toCharacterUiModel() }
}

fun Character.toCharacterUiModel(): CharacterUiModel {
    return CharacterUiModel(
        uid = uid,
        imageUrl = imageUrl,
        primaryDescription = name,
        secondaryDescription = species
    )
}

fun Character.toCharacterDetailsUiModel(): CharacterDetailsUiModel {
    return CharacterDetailsUiModel(
        uid = uid,
        name = name,
        imageUrl = imageUrl,
        status = status,
        species = species,
        type = type,
        gender = gender,
        episodes = episodes,
        origin = origin,
        location = location
    )
}