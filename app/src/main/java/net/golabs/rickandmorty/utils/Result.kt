package net.golabs.rickandmorty.utils

sealed class Result<T>(val data: T? = null, val throwable: Throwable? = null) {
    class Loading<T> : Result<T>()
    class Success<T>(data: T) : Result<T>(data)
    class Error<T>(throwable: Throwable) : Result<T>(throwable = throwable)
}