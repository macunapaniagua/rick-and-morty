package net.golabs.rickandmorty.utils

import androidx.recyclerview.widget.DiffUtil
import net.golabs.rickandmorty.models.ui.CharacterUiModel

class CharactersDiffUtil(
    private val oldList: List<CharacterUiModel>,
    private val newList: List<CharacterUiModel>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.count()

    override fun getNewListSize(): Int = newList.count()

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].uid == newList[newItemPosition].uid
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}