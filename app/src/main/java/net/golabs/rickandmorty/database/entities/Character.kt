package net.golabs.rickandmorty.database.entities

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "characters")
data class Character(
    @PrimaryKey val uid: Int,
    val name: String,
    @ColumnInfo(name = "image_url")
    val imageUrl: String,
    val status: String,
    val species: String,
    val type: String,
    val gender: String,
    val episodes: List<String>,
    @Embedded(prefix = "origin_")
    val origin: Place,
    @Embedded(prefix = "location_")
    val location: Place
)