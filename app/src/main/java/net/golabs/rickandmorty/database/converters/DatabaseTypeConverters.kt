package net.golabs.rickandmorty.database.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class DatabaseTypeConverters {

    @TypeConverter
    fun fromListOfStrings(list: List<String>): String {
        return Gson().toJson(list)
    }

    @TypeConverter
    fun toListOfStrings(string: String): List<String> {
        return Gson().fromJson(string, object: TypeToken<List<String>>() {}.type)
    }
}