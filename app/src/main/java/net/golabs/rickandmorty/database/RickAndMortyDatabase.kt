package net.golabs.rickandmorty.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import net.golabs.rickandmorty.database.converters.DatabaseTypeConverters
import net.golabs.rickandmorty.database.daos.CharacterDao
import net.golabs.rickandmorty.database.entities.Character

@Database(entities = [Character::class], version = 1)
@TypeConverters(DatabaseTypeConverters::class)
abstract class RickAndMortyDatabase: RoomDatabase() {
    abstract fun characterDao(): CharacterDao

    companion object {

        @Volatile
        private var INSTANCE: RickAndMortyDatabase? = null
        private const val DATABASE_NAME = "rick_and_morty_database"

        fun getDataBase(context: Context): RickAndMortyDatabase {
            return INSTANCE ?: synchronized(this) {
                Room.databaseBuilder(
                    context,
                    RickAndMortyDatabase::class.java,
                    DATABASE_NAME
                ).build().also {
                    INSTANCE = it
                }
            }
        }

    }
}