package net.golabs.rickandmorty.database.daos

import androidx.room.*
import net.golabs.rickandmorty.database.entities.Character
import net.golabs.rickandmorty.utils.Constants

@Dao
interface CharacterDao {

    @Query("SELECT * FROM characters LIMIT :perPage OFFSET ((:page - 1) * :perPage)")
    fun getAllCharacters(page: Int, perPage: Int = Constants.ITEMS_PER_PAGE): List<Character>

    @Query("SELECT * FROM characters WHERE uid = :uid")
    fun getCharacterByUid(uid: Int): Character

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCharacters(characters: List<Character>)

}