package net.golabs.rickandmorty.database.entities

import androidx.room.ColumnInfo

data class Place(
    val name: String,
    @ColumnInfo(name = "place_url")
    val placeUrl: String
)