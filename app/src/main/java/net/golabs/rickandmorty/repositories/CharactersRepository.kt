package net.golabs.rickandmorty.repositories

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import net.golabs.rickandmorty.database.daos.CharacterDao
import net.golabs.rickandmorty.database.entities.Character
import net.golabs.rickandmorty.models.domain.PagedList
import net.golabs.rickandmorty.network.WebService
import net.golabs.rickandmorty.network.dtos.AllCharactersDto
import net.golabs.rickandmorty.network.dtos.InfoDto
import net.golabs.rickandmorty.utils.Result
import net.golabs.rickandmorty.utils.mappers.toCharacters
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CharactersRepository(
    private val webService: WebService,
    private val characterDao: CharacterDao
) {

    fun getCharacterByUid(uid: Int): LiveData<Character> {
        val character = MutableLiveData<Character>()
        Thread {
            character.postValue(characterDao.getCharacterByUid(uid))
        }.start()
        return character
    }

    private fun fetchCharactersFromDatabase(page: Int, callback: (Result<PagedList<Character>>) -> Unit) {
        Thread {
            val characters = characterDao.getAllCharacters(page)
             val pagedList = PagedList(
                 currentPage = page,
                 nextPage = extractNextPage(page, characters),
                 items = characters
             )
            callback(Result.Success(pagedList))
        }.start()
    }

    fun getAllCharacters(page: Int): LiveData<Result<PagedList<Character>>> {
        // Create LiveData with Loading as default value
        val result: MutableLiveData<Result<PagedList<Character>>> = MutableLiveData(Result.Loading())

        // Network call to get Characters
        webService.getAllCharacters(page).enqueue(object: Callback<AllCharactersDto> {

            override fun onFailure(call: Call<AllCharactersDto>, t: Throwable) {
                // Network Failure.. Try to fetch data from local database
                Log.d(TAG, t.message ?: "Unknown error")
                fetchCharactersFromDatabase(page) {
                    result.postValue(it)
                }
            }

            override fun onResponse(call: Call<AllCharactersDto>, response: Response<AllCharactersDto>) {
                if (response.isSuccessful) {
                    response.body()?.let { body ->
                        // Store response in local database and return it to the user
                        Thread {
                            val charactersResponse = body.results.toCharacters()
                            characterDao.insertCharacters(charactersResponse)
                            val pagedList = PagedList(
                                currentPage = page,
                                nextPage = extractNextPage(page, body.info),
                                items = charactersResponse
                            )
                            result.postValue(Result.Success(pagedList))
                        }.start()
                    }
                    return
                }

                // Response Error (404, 500, etc).. Try to fetch data from local database
                Log.d(TAG, response.message())
                fetchCharactersFromDatabase(page) {
                    result.postValue(it)
                }
            }

        })
        return result
    }

    private fun extractNextPage(currentPage: Int, info: InfoDto): Int? {
        return if (currentPage == info.pages) null else currentPage.plus(1)
    }

    private fun extractNextPage(currentPage: Int, characters: List<Character>): Int? {
        return if (characters.isEmpty()) null else currentPage.plus(1)
    }

    companion object {
        private const val TAG = "CharactersRepository"
    }

}