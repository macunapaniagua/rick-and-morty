<?xml version="1.0" encoding="utf-8"?>
<androidx.cardview.widget.CardView xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:layout_gravity="center"
    app:cardCornerRadius="@dimen/character_details_corner_radius"
    tools:context=".screens.characters.CharacterDetailsFragment">

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="@dimen/character_details_card_width"
        android:layout_height="wrap_content"
        android:padding="@dimen/character_details_card_padding">

        <ImageView
            android:id="@+id/characterDetailsImage"
            android:layout_width="@dimen/character_details_image_size"
            android:layout_height="@dimen/character_details_image_size"
            android:adjustViewBounds="true"
            android:contentDescription="@null"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            tools:src="@mipmap/ic_launcher" />

        <TextView
            android:id="@+id/characterDetailsNameText"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:gravity="center_horizontal"
            android:lines="1"
            android:textColor="@color/primary_text_color"
            android:textSize="@dimen/character_details_name_text_size"
            android:textStyle="bold"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/characterDetailsImage"
            tools:text="Rick Sanchez" />

        <TextView
            android:id="@+id/characterDetailsStatusText"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:lines="1"
            android:textColor="@color/secondary_text_color"
            android:textSize="@dimen/character_details_status_text_size"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@id/characterDetailsNameText"
            tools:text="Alive" />

        <androidx.constraintlayout.widget.Group
            android:id="@+id/characterDetailsSpeciesGroup"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:constraint_referenced_ids="characterDetailsSpeciesText,characterDetailsSpeciesLabel" />

        <TextView
            android:id="@+id/characterDetailsSpeciesLabel"
            style="@style/Subtitle"
            android:text="@string/specie"
            app:layout_constraintBaseline_toBaselineOf="@+id/characterDetailsSpeciesText"
            app:layout_constraintStart_toStartOf="parent" />

        <TextView
            android:id="@+id/characterDetailsSpeciesText"
            style="@style/CharacterDetails"
            android:layout_marginTop="@dimen/character_details_specie_margin_top"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/barrier"
            app:layout_constraintTop_toBottomOf="@+id/characterDetailsStatusText"
            tools:text="Specie" />

        <androidx.constraintlayout.widget.Group
            android:id="@+id/characterDetailsTypeGroup"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:constraint_referenced_ids="characterDetailsTypeLabel,characterDetailsTypeText" />

        <TextView
            android:id="@+id/characterDetailsTypeLabel"
            style="@style/Subtitle"
            android:text="@string/subSpecie"
            app:layout_constraintBaseline_toBaselineOf="@+id/characterDetailsTypeText"
            app:layout_constraintStart_toStartOf="parent" />

        <TextView
            android:id="@+id/characterDetailsTypeText"
            style="@style/CharacterDetails"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/barrier"
            app:layout_constraintTop_toBottomOf="@+id/characterDetailsSpeciesText"
            tools:text="Subspecie" />

        <androidx.constraintlayout.widget.Group
            android:id="@+id/characterDetailsGenderGroup"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:constraint_referenced_ids="characterDetailsGenderLabel,characterDetailsGenderText" />

        <TextView
            android:id="@+id/characterDetailsGenderLabel"
            style="@style/Subtitle"
            android:text="@string/gender"
            app:layout_constraintBaseline_toBaselineOf="@+id/characterDetailsGenderText"
            app:layout_constraintStart_toStartOf="parent" />

        <TextView
            android:id="@+id/characterDetailsGenderText"
            style="@style/CharacterDetails"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/barrier"
            app:layout_constraintTop_toBottomOf="@+id/characterDetailsTypeText"
            tools:text="Gender" />

        <androidx.constraintlayout.widget.Group
            android:id="@+id/characterDetailsOriginGroup"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:constraint_referenced_ids="characterDetailsOriginLabel,characterDetailsOriginText" />

        <TextView
            android:id="@+id/characterDetailsOriginLabel"
            style="@style/Subtitle"
            android:text="@string/origin"
            app:layout_constraintBaseline_toBaselineOf="@+id/characterDetailsOriginText"
            app:layout_constraintStart_toStartOf="parent" />

        <TextView
            android:id="@+id/characterDetailsOriginText"
            style="@style/CharacterDetails"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/barrier"
            app:layout_constraintTop_toBottomOf="@+id/characterDetailsGenderText"
            tools:text="Origin" />

        <androidx.constraintlayout.widget.Group
            android:id="@+id/characterDetailsLastKnownLocationGroup"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:constraint_referenced_ids="characterDetailsLastKnownLocationText,characterDetailsLastKnownLocationLabel" />

        <TextView
            android:id="@+id/characterDetailsLastKnownLocationLabel"
            style="@style/Subtitle"
            android:text="@string/location"
            app:layout_constraintBaseline_toBaselineOf="@+id/characterDetailsLastKnownLocationText"
            app:layout_constraintStart_toStartOf="parent" />

        <TextView
            android:id="@+id/characterDetailsLastKnownLocationText"
            style="@style/CharacterDetails"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toEndOf="@+id/barrier"
            app:layout_constraintTop_toBottomOf="@+id/characterDetailsOriginText"
            tools:text="Location" />

        <androidx.constraintlayout.widget.Barrier
            android:id="@+id/barrier"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            app:barrierDirection="end"
            app:constraint_referenced_ids="characterDetailsOriginLabel,characterDetailsGenderLabel,characterDetailsTypeLabel,characterDetailsSpeciesLabel,characterDetailsLastKnownLocationLabel" />

    </androidx.constraintlayout.widget.ConstraintLayout>

</androidx.cardview.widget.CardView>